import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProductComponent } from './components/product/product.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ProductComponent
  ],
  imports: [
    CommonModule
  ],
  exports : [HeaderComponent, FooterComponent, ProductComponent]
})
export class SharedModule { }
