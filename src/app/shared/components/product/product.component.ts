import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../models/product';

@Component({
  selector: 'onet-product',
  templateUrl: './product.component.html',
})
export class ProductComponent implements OnInit {



  @Input() phone: Product;


  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  navigate(): void {
    this.router.navigate(['home/detail'], {queryParams: {name: this.phone.name, brand: this.phone.brand}});
  }

}
