import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'onet-product-detail',
  templateUrl: './product-detail.component.html',
})
export class ProductDetailComponent implements OnInit {
  brand: any;
  name: any;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.brand = this.route.snapshot.queryParams['brand'];
    this.name =  this.route.snapshot.queryParams['name']
    console.log(this.brand);

  }

}
