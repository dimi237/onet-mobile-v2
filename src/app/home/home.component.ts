import { Component, OnInit } from '@angular/core';
import { Product } from '../shared/models/product';

@Component({
  selector: 'onet-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  items = [
    { path: '../../assets/images/round-phone_iphone-24px.png', name: 'Smartphones' },
    { path: '../../assets/images/round-headset-24px.png', name: 'Accessories' },
    { path: '../../assets/images/round-tablet_mac-24px.png', name: 'Tablets' },
    { path: '../../assets/images/Group(1).png', name: 'Sale!' },

  ];
  arrivals = [
    { path: '../../assets/images/Group(3).png', name: 'Android phones' },
    { path: '../../assets/images/Group(2).png', name: 'iPhones' }
  ];

  info = [
    { path: '../../assets/images/Vector(4).png', name: 'Call us', value: '+242 06 908 3535' },
    { path: '../../assets/images/Vector(5).png', name: 'Shipping', value: 'Everywhere in Congo' },
    { path: '../../assets/images/Vector(6).png', name: 'Work with us', value: 'Become our consultant' },
  ];

  brand = [
    { path: '../../assets/images/apple.png', class: 'brand-item bordered' },
    { path: '../../assets/images/samsung.png', class: 'brand-item bordered' },
    { path: '../../assets/images/xiaomi.png', class: 'brand-item bordered' },
    { path: '../../assets/images/huawei.png', class: 'brand-item bordered' },
    { path: '../../assets/images/infinix.png', class: 'brand-item bordered' },
    { path: '../../assets/images/alcatel.png', class: 'brand-item' }

  ];

  tips = [
    { path: '../../assets/images/icon1.png', class: 'brand-item bordered', value: 'Fast delivery' },
    { path: '../../assets/images/icon2.png', class: 'brand-item bordered', value: 'Best prices in Congo' },
    { path: '../../assets/images/icon3.png', class: 'brand-item bordered', value: 'Warranty and post warranty service' },
    { path: '../../assets/images/icon4.png', class: 'brand-item bordered', value: 'Guaranteed return and exchange' },
    { path: '../../assets/images/icon5.png', class: 'brand-item ', value: 'Certified products' },
  ];
  phones: Product[] = [{
    name: 'Redmi 6A Dual SIM 16GB HDD - 2GB RAM - Gold- 12 Months ...',
    brand: 'Xiaomi',
    ratings: { rating: 4, reviews: 36 },
    prices: { price: 99900, discount: 40 },
    otherImages: ['../../../../assets/images/phone1.png']
  },
  {
    name: 'C Note Dual SIM 32GB HDD - 3GB RAM - 13MP - Gold - 12 Months',
    brand: 'UMIDIGI',
    ratings: { rating: 4, reviews: 36 },
    prices: { price: 99900, discount: 33 },
    otherImages: ['../../../../assets/images/phone2.png']
  },
  {
    name: 'Rise 53 Dual SIM 8GB HDD - White - 12 Months',
    brand: 'Orange',
    ratings: { rating: 4, reviews: 36 },
    prices: { price: 48000, discount: 38 },
    otherImages: ['../../../../assets/images/phone3.png']
  },
  {
    name: 'iPhone 8 64GB HDD - Gold  - White - 12 Months',
    brand: 'Apple',
    ratings: { rating: 4, reviews: 36 },
    prices: { price: 490000, discount: 30 },
    otherImages: ['../../../../assets/images/phone4.png']
  },
  {
    name: 'Galaxy S7 Edge 32GB HDD - 4GB RAM - Gold - 12 Months + Cover...',
    brand: 'Samsung',
    ratings: { rating: 4, reviews: 36 },
    prices: { price: 250000, discount: 40 },
    otherImages: ['../../../../assets/images/phone5.png']
  }];

  phone = this.phones[0];

  constructor() { }

  ngOnInit(): void {
  }

}
