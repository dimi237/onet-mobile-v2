import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { CartModalComponent } from './cart-modal/cart-modal.component';
import { SearchModalComponent } from './search-modal/search-modal.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    HomeComponent,
    ProductDetailComponent,
    CartModalComponent,
    SearchModalComponent,
    SearchResultComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
  ]
})
export class HomeModule { }
