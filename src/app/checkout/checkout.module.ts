import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckoutRoutingModule } from './checkout-routing.module';
import { CheckoutComponent } from './checkout.component';
import { Checkout1Component } from './checkout1/checkout1.component';
import { Checkout2Component } from './checkout2/checkout2.component';
import { Checkout3Component } from './checkout3/checkout3.component';
import { Checkout4Component } from './checkout4/checkout4.component';



@NgModule({
  declarations: [
    CheckoutComponent,
    Checkout1Component,
    Checkout2Component,
    Checkout3Component,
    Checkout4Component,
  ],
  imports: [
    CommonModule,
    CheckoutRoutingModule
  ]
})
export class CheckoutModule { }
