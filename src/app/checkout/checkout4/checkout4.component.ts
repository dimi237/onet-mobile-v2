import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'onet-checkout4',
  templateUrl: './checkout4.component.html',
  styles: [
  ],
  encapsulation: ViewEncapsulation.None
})
export class Checkout4Component implements OnInit {

mobileMoney: boolean= false;
cashOnDelivery: boolean= false;

  constructor() { }

  ngOnInit(): void {
  }

  activateMobileMoney = () => {
this.mobileMoney= true
this.cashOnDelivery = false
console.log(this.mobileMoney);
  }
  activateCashOnDelivery = () => {
this.cashOnDelivery = true
this.mobileMoney= false
  }

}
