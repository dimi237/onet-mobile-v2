import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { CheckoutComponent } from './checkout.component';
import { Checkout1Component } from './checkout1/checkout1.component';
import { Checkout2Component } from './checkout2/checkout2.component';
import { Checkout3Component } from './checkout3/checkout3.component';
import { Checkout4Component } from './checkout4/checkout4.component';


const routes: Routes = [
  { path: '', component: Checkout1Component },
  {
    path: 'checkout2',
    component: Checkout2Component,
  },
  {
    path: 'checkout3',
    component: Checkout3Component,
  },
  {
    path: 'checkout4',
    component: Checkout4Component,
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckoutRoutingModule {}
