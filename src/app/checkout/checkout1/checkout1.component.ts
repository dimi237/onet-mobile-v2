import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'onet-checkout1',
  templateUrl: './checkout1.component.html',
  encapsulation: ViewEncapsulation.None
})
export class Checkout1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
