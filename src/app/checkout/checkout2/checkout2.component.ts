import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'onet-checkout2',
  templateUrl: './checkout2.component.html',
  styles: [
  ],
  encapsulation: ViewEncapsulation.None
})
export class Checkout2Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
