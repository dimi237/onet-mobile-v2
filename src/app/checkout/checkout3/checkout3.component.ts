import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'onet-checkout3',
  templateUrl: './checkout3.component.html',
  styles: [
  ],
  encapsulation: ViewEncapsulation.None
})
export class Checkout3Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
