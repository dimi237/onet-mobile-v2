import { Component } from '@angular/core';

@Component({
  selector: 'onet-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'onet-mobile';
}
